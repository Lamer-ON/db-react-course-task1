import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

const initialState = {
    // value: '',
    // lat: null,
    // lon: null,
    value: 'Kharkiv',
    lat: 49.98081,
    lon: 36.25272,
    loading: false,
    loading2: false,
    dataCity: [],
    data: [],
    wind: [],
    temp: '',
    humidity: ''
}

export const getWeather = createAsyncThunk(
    'weather/getWeather',
    async (thunkAPI) => {
        const res = await fetch(thunkAPI).then(
            (data) => data.json()
        )
        return res
    })

export const getCities = createAsyncThunk(
    'weather/getCities',
    async () => {
        const res = await fetch(
            `https://parseapi.back4app.com/classes/City?limit=15&order=-population&keys=name,population,location`,
            {
                headers: {
                    'X-Parse-Application-Id': 'WHhatLdoYsIJrRzvkD0Y93uKHTX49V9gmHgp8Rw3',
                    'X-Parse-Master-Key': 'iyzSAHUmPKUzceWRIIitUD1OKAGHvVUzEYb5DCpj',
                }
            }
        ).then(
            (data) => data.json()
        )
        console.log(res);
        return res
    })

export const postSlice = createSlice({
    name: 'weather',
    initialState,
    reducers: {
        select: (state, action) => {
            state.value = action.payload
        },
        setCityLat: (state, action) => {
            state.lat = action.payload
        },
        setCityLon: (state, action) => {
            state.lon = action.payload
        },
    },
    extraReducers: {

        [getWeather.pending]: (state) => {
            state.loading = true
        },
        [getWeather.fulfilled]: (state, { payload }) => {
            state.loading = false
            state.data = payload
        },
        [getWeather.rejected]: (state) => {
            state.loading = false
        },


        [getCities.pending]: (state) => {
            state.loading2 = true
        },
        [getCities.fulfilled]: (state, { payload }) => {
            state.loading2 = false
            state.dataCity = payload
        },
        [getCities.rejected]: (state) => {
            state.loading2 = false
        },
    },
})

export const { select, setCityLat, setCityLon, setData } = postSlice.actions

export const postReducer = postSlice.reducer