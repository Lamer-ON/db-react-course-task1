import { configureStore } from '@reduxjs/toolkit';
import { postReducer } from './features/weather/weatherSlice';

export default configureStore({
  reducer: {
    weather: postReducer
  }
})  