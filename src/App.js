import React, { useState} from "react";
import { Route, Routes} from 'react-router-dom';

import PrivateRoute from "./utils/PrivateRoute";
import Home from './pages/Home';
import Signup from "./pages/SignUp";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import TodoPage from "./pages/TodoPage";
import WeatherPage from "./pages/WeatherPage";
import "./App.scss";

function App() {

  const [isAuth, setIsAuth] = useState(JSON.parse(localStorage.getItem("userData")));

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/login" element={<Login setIsAuth={setIsAuth} />} />
      <Route path="/dashboard" element={<PrivateRoute isAuth={isAuth}><Dashboard /></PrivateRoute>} />
      <Route path="/todo" element={<TodoPage />} />
      <Route path="/weather" element={<WeatherPage />} />
    </Routes>
  );
}

export default App;
