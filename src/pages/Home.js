import React from 'react';

import Header from '../components/Header';

import './../scss/common.scss';
import './../scss/home.scss';

export default function Home() {
    return (
        <>
        <Header />
            <main className="home-page main container">
                <div className="wrapper">
                <h1>HOME PAGE</h1>
                </div>
            </main>
        </>
    )
}
