import React from "react";

import Signup from "../components/Signup/Signup";
import Header from "../components/Header";

export default function SignUpPage () {
    return (
        <>
        <Header />
        <Signup />
        </>
    )
}