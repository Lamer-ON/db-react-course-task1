import React from 'react';

import Header from '../components/Header';
import Weather from '../components/Weather';

import './../scss/common.scss';
import './../scss/weatherPage.scss';

export default function WeatherPage() {

    return (
        <>
            <Header />
            <main className="weather-page main container">
                <div className="wrapper">
                    <h1>Weather Page</h1>
                    <Weather />
                </div>
            </main>
        </>
    )
}
