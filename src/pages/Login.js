import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';

import Header from '../components/Header';
import Input from '../components/Signup/Input/Input';
import configForm from './configForm';
import Api from '../utils/Api';

import './../scss/common.scss';
import './../scss/login.scss';
import "./../components/Signup/input.scss";

export default function Login({ setIsAuth }) {
    const initialState = { login: "", password: "" };
    const [state, setState] = useState(initialState);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setState((field) => {
            return { ...field, [name]: value };
        });
    };

    const handleSubmit = (event) => {
        console.log(`Login data:
        login: ${state.login}
        password: ${state.password}
        `);
        event.preventDefault();
        Api.signin().then((userData) => {
            if (JSON.stringify(userData) !==
                JSON.stringify(state)) {
                console.log("Bad data !!!");
                setIsAuth(false);
            } else {
                console.log("Welcome!!!");
                setIsAuth(true);
                <Navigate to={"/"} />
            }
        })
        setState(initialState);
    };

    return (
        <>
            <Header />
            <main className="login-page main container">
                <div className="wrapper">
                    <form onSubmit={handleSubmit}>
                        <h1>Sign in</h1>
                        {configForm.map((current, index) => {
                            return (
                                <Input
                                    value={state[current.name]}
                                    name={current.name}
                                    type={current.type}
                                    labelTitle={current.label}
                                    key={`${current.name}_${index}`}
                                    handleInputChange={handleChange}
                                />
                            );
                        })}
                        <input className="btn btn-primary" type="submit" value="Submit" />
                    </form>
                </div>

            </main>
        </>
    )
}