import React from 'react';

import Header from '../components/Header';
import Todo from '../components/Todo';

import './../scss/common.scss';
import './../scss/todoPage.scss';

export default function TodoPage() {

    return (
        <>
            <Header />
            <main className="todo-page main container">
                <div className="wrapper">
                    <h1>TODO PAGE</h1>
                    <Todo />
                </div>
            </main>
        </>
    )
}
