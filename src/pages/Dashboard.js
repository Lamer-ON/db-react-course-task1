import React from 'react';

import Header from '../components/Header';
import FormBuilder from '../components/FormBuilder/FormBuilder';
import {default as fields} from '../components/FormBuilder/configBuilder';

import './../scss/common.scss';
import './../scss/dashboard.scss';

export default function Dashboard() {
    return (
        <>
        <Header />
            <main className="dashboard-page main container">
                <div className="wrapper">
                <h1>Dashboard page</h1>
                <  FormBuilder fields={fields}/>
                </div>
            </main>
        </>
    )
}