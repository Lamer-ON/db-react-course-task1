const validate = {
    nameLength: (value, min = 3, max = 10) => {
        if (value.length >= min && value.length <= max) {
            return { valid: true, error: "" };
        }
        return { valid: false, error: `Need from ${min} to ${max} characters` };
    },
    email: (value) => {
        const regular =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regular.test(value)) {
            return { valid: true, error: "" };
        }
        return { valid: false, error: "Must be a valid email" };
    },
    pass: (value, min = 6) => {
        const regular =
            /(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]/g;
        // if (regular.test(value) && value.length >= min) {
        if (regular.test(value)) {
            return {  valid: true, error: "" };
        }
        return {  valid: false, error: "Invalid password" };
    },
    confirmPassword: (pass1, pass2) => {
        if (pass1 === pass2) {
            return { valid: true, error: "" };
        }
        return { valid: false, error: "Password mismatch" };
    },
    minLength: (value, min = 3) => {
        if (value.length >= min) {
            return { valid: true, error: "" };
        }
        return { valid: false, error: `Need min ${min} characters` };
    },
    maxLength: (value, max = 6) => {
        if (value.length <= max) {
            return { valid: true, error: "" };
        }
        return { valid: false, error: `Need max ${max} characters` };
    },
}

export default validate;