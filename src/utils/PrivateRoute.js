import React from "react";
import { Navigate } from 'react-router-dom';

const PrivateRoute = (props) => {
    const { children, isAuth } = props;

    if (!isAuth) {
        return <Navigate to={"/login"} />
    }
    return children;
}

export default PrivateRoute;