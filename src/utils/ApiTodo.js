const user = "todos";
const ApiTodo = {

    getTodos: () => {
        return new Promise(function (resolve, reject) {
            setTimeout(() => {
                let userData = JSON.parse(localStorage.getItem(user) || []);
                resolve(userData);
            }, 1500);
        })
    },

    setTodos: (userData) => {
        setTimeout(() => {
            const data = localStorage.setItem(user, JSON.stringify(userData));
            return data;
        }, 1500);
    },
};

export default ApiTodo;