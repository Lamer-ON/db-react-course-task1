import React from "react";

import './select.scss';

const Select = (props) => {
    return (
        <p className="field">
            <label>{props.labelTitle}</label>

            <select
                value={props.value}
                onChange={props.handleInputChange}>

                {props.options.map((current, index) => {
                    return <option
                        value={current.value}
                        key={`${current.label}_${index}`}
                        >
                        {current.label}
                    </option>;
                })}
            </select>
        </p>
    );
}

export default Select;