import React, { useState, useEffect } from "react";

import Input from "../Signup/Input/Input";
import Select from "./select/Select";
import validate from "../../utils/formBuilderUtils";

import "./formbuilder.scss";
import "./select/select.scss"


const FormBuilder = (props) => {
    const fields = props.fields;
    const inputs = fields.filter((e) => {
        return e.type === 'text';
    });

    let initialState = {};
    for (const item of fields) {
        if (item['name']) {
            initialState[item['name']] = "";
        }
    }

    let defaultValidation = {};
    for (const item of inputs) {
        if (item['name']) {
            defaultValidation[item['name']] = [{ valid: false, error: " " }];
        }
    }

    const [state, setState] = useState(initialState);
    const [validationState, setValidation] = useState(defaultValidation);
    const [formValid, setFormValid] = useState(false);


    useEffect(() => {
        setFormValid(Object.values(validationState).every((val) => !val.error));
    }, [validationState, formValid]);


    useEffect(() => {
        if (formValid) {
            setState(initialState);
            setValidation(defaultValidation);
        }
    }, [formValid]);

    const selects = fields.filter((e) => {
        return e.type === 'select';
    });

    let validationMethods = [];
    for (const item of inputs) {
        if (item['validations']) {
            let method = item['validations'].onChange[0]['name'];
            item['validations']['onChange'].map((current, index) => {

                validationMethods[item['name']] = current;
            });

        }
    }


    let { name: method, minLength: digit } = {
        name: 'minLength',
        minLength: 6,
    };
    let { name, minLength } = {
        name: 'minLength',
        minLength: 6,
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setValidation({
            code: validate.minLength(state.code),
            password: validate.pass(state.password),
            // password: [validate.pass(state.password), validate.minLength(state.password)],
            confirmPassword: validate.confirmPassword(state.password, state.confirmPassword),
        });
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setState((field) => {
            return { ...field, [name]: value };
        });
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h2>Form Builder</h2>

                {inputs.map((current, index) => {
                    return (
                        <Input
                            value={state[current.name]}
                            name={current.name}
                            type={current.type}
                            labelTitle={current.name}
                            key={`${current.name}_${index}`}
                            handleInputChange={handleChange}
                            InputError={validationState[current.name].error}
                        />
                    );
                })}

                {selects.map((current, index) => {
                    return (
                        <Select
                            value={state[current.name]}
                            name={current.name}
                            type={current.type}
                            labelTitle={current.name}
                            key={`${current.name}_${index}`}
                            handleInputChange={handleChange}
                            options={current.options}
                        />
                    );
                })}


                <input className="btn btn-primary" type="submit" value="Submit" />
            </form>

        </>
    );
};

export default FormBuilder;