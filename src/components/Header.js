import React from 'react'
import { Link, useLocation } from 'react-router-dom';

import './../scss/header.scss';
import imgHome from './../img/home-icon.png';

function Header() {
  const location = useLocation();
  const path = location.pathname;

  return (
    <header className="header">

      <Link to={"/"} className={`link-back ${(path === "/") ? "disabled" : ""}`}>
            <img src={imgHome} alt="home-icon.png" />
      </Link>

      <Link to={"/dashboard"} className={`nav-link ${(path === "/dashboard") ? "active-link" : ""}`}>
      Dashboard
      </Link>
      <Link to={"/todo"} className={`nav-link ${(path === "/todo") ? "active-link" : ""}`}>
      Todo
      </Link>
      <Link to={"/weather"} className={`nav-link ${(path === "/weather") ? "active-link" : ""}`}>
      Weather
      </Link>
      
      <Link to={"/login"} className={`nav-link ${(path === "/login") ? "active-link" : ""}`}>
      Sign in
      </Link>

      <Link to={"/signup"} className={`nav-link ${(path === "/signup") ? "active-link" : ""}`}>
        Sign up
      </Link>

    </header>
  );
}

export default Header;
