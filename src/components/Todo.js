import React, { useState, useEffect } from 'react'
import TodoList from './TodoList';
import Input from './Signup/Input/Input';
import { Context } from '../context';

import ApiTodo from '../utils/ApiTodo';
import './../scss/header.scss';
import './todo.scss';

function Todo() {

  const [todos, setTodos] = useState([]);
  const [todoTitle, setTodoTitle] = useState("");
  const [edit, setEdit] = useState("");
  const [editId, setEditId] = useState("");


  useEffect(() => {
    ApiTodo.getTodos().then(userData => setTodos(userData));
  }, []);

  useEffect(() => {
    ApiTodo.setTodos(todos)
  }, [todos]);

  const addTodoClick = () => {
    if (todoTitle) {
      setTodos([...todos,
      {
        id: Date.now(),
        title: todoTitle,
        completed: false
      }
      ])
      setTodoTitle("");
    }
  }

  const addTodo = e => {
    if (e.key === 'Enter' && todoTitle) {
      setTodos([...todos,
      {
        id: Date.now(),
        title: todoTitle,
        completed: false
      }
      ])
      setTodoTitle("");
    }
  }

  const removeTodo = id => {
    setTodos(todos.filter(todo => {
      return todo.id !== id
    }))
  }

  const toggleTodo = id => {
    setTodos(todos.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    }))
  }

  const editTodo = id => {
    document.getElementById("modal").style.display = "block";
    setEditId(id);
    todos.map(todo => {
      if (todo.id === id) {
        setEdit(todo.title);
      }
      return todo;
    })
  }

  const enterChangeTodo = e => {
    if (e.key === 'Enter' && edit) {
      todos.map(el => {
        if (el.id === editId) {
          el.title = edit;
          setTodos([...todos]);
        }
        return todos;
      })
      document.getElementById("modal").style.display = "none";
    }
  }

  const handleChange = (e) => {
    setTodoTitle(e.target.value);
  };

  const changeTodo = (e) => {
    setEdit(e.target.value)
  };

  return (
    <Context.Provider value={
      { removeTodo, toggleTodo, editTodo }
    }>
      <div className="todo-container">
        <h2>TODO</h2>

        <div className="input-field">
          <Input
            name={"todo-title"}
            value={todoTitle}
            type="text"
            labelTitle='Input new Todo name and press Enter or click "Add"'
            handleInputChange={handleChange}
            keyPress={addTodo}
          />

          <button
            className='add-todo-btn'
            onClick={addTodoClick}>
            Add
          </button>
        </div>

        <TodoList todos={todos} />

        <div className="modal" id='modal'>
          <h3>Change Todo & press Enter</h3>
          <Input
            name={"modal-input"}
            value={edit}
            type="text"
            placeholder='Change todo title'
            handleInputChange={changeTodo}
            keyPress={enterChangeTodo}
          />
        </div>

      </div>
    </Context.Provider>
  );
}

export default Todo;
