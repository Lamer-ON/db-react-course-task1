import React, { useEffect } from 'react'

import { useSelector, useDispatch } from 'react-redux'
import { getWeather, getCities, setCityLat, setCityLon } from './../redux/features/weather/weatherSlice';
import SelectSity from './SelectSity';

import './../scss/header.scss';
import './weather.scss';

function Whether() {

  const dispatch = useDispatch()
  // const wind = useSelector(state => state.weather.wind)
  // const temp = useSelector(state => state.weather.temp)
  // const humidity = useSelector(state => state.weather.humidity)
  // const loading2 = useSelector(state => state.loading2)

  const dataCity = useSelector(state => state.weather.dataCity)
  const city = useSelector(state => state.weather.value)
  const daily = useSelector(state => state.weather.data.daily)
  const loading = useSelector(state => state.loading)
  const cityArr = useSelector(state => state.weather.dataCity.results)

  const cityLat = useSelector(state => state.weather.lat)
  const cityLon = useSelector(state => state.weather.lon);

  let url = `http://api.openweathermap.org/data/2.5/onecall?lat=${cityLat}&lon=${cityLon}&units=metric&cnt=10&appid=22f5052a329a964e69cb79c599eee95c`;
  // let currentCityArr;


  useEffect(() => {
    dispatch(getCities())
  }, [])
  
  useEffect(() => {
  let currentCityArr = cityArr.filter((cityItem) => {
        return cityItem.name === city
      })
    dispatch(setCityLat(currentCityArr[0].location.latitude))
    dispatch(setCityLon(currentCityArr[0].location.longitude))
  }, [city])
  // }, [cityArr, dataCity, city, dispatch])

    
  useEffect(() => {
    dispatch(getWeather(url))
  }, [city, url])


  if (loading) return <p>Loading...</p>

  return (
    <div className="weather-container">
      <h2>Weather</h2>
      <h3>{city}</h3>
      <SelectSity />
      <div className="weather-card-wrap">

        {daily && !loading &&
          daily.map((data, i) => {
            return (
              <div className="weather-card" key={i}>
                <p>Min temp: {Math.floor(data.temp.min)} &#xb0;C</p>
                <p>Max temp: {Math.floor(data.temp.max)} &#xb0;C</p>
                <p>Humidity: {Math.floor(data.humidity)} %</p>
                <p>Wind speed: {Math.floor(data.wind_speed)} m/sec</p>
                <img src={`http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`} alt="weather-icon" />
              </div>
            )
          })
        }
      </div>

    </div>
  );
}

export default Whether;
