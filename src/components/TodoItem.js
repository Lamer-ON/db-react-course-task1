import React, { useState, useContext } from "react";
import { Context } from "../context";

export default function TodoItem({ title, id, completed }) {

    const { toggleTodo, removeTodo, editTodo } = useContext(Context);
    const classes = ["todo"];

    if (completed) {
        classes.push("completed");
    }

    return (
        <li className={classes.join(" ")}>
            <label>
                <input
                    type="checkbox"
                    checked={completed}
                    onChange={() => toggleTodo(id)} />
                <span>{title}</span>
            </label>

            <button onClick={() => editTodo(id)}
                className='edit-todo-btn btn'>Edit</button>

            <button onClick={() => removeTodo(id)}
                className="delete-todo-btn btn">Delete</button>
        </li>
    );
}
