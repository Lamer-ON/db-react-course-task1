import React from "react";

import './button.scss';

const Button = ({
    children, onClick, className, disabled, ...attrs
}) => {
    return (
        <button
            className={className}
            disabled={disabled}
            onClick={onClick}
            {...attrs}
        >
            {children}
        </button>

    )
}

export default Button;