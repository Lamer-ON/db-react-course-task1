const configForm = [
  {
    label: "Input name",
    name: "name",
    type: "text",
  },
  {
    label: "Input email",
    name: "email",
    type: "email",
  },
  {
    label: "Input password",
    name: "password",
    type: "password",
  },
  {
    label: "Confirm Password",
    name: "confirmPassword",
    type: "password",
  },
  
];

export default configForm;