import React, { useState, useEffect } from "react";

import Input from "./Input/Input";
import validate from "./../../utils/utils";
import configForm from "./configForm";
import Api from "../../utils/Api";

import "./signup.scss";
import "./input.scss";

const initialState = { name: "", email: "", password: "", confirmPassword: "" };
const defaultValidation = {
    name: { valid: false, error: " " },
    email: { valid: false, error: " " },
    password: { valid: false, error: " " },
    confirmPassword: { valid: false, error: " " },
};

const Signup = () => {
    const [state, setState] = useState(initialState);
    const [validationState, setValidation] = useState(defaultValidation);
    const [formValid, setFormValid] = useState(false);
    
    useEffect(() => {
        setFormValid(Object.values(validationState).every((val) => !val.error));
    }, [validationState, formValid]);

    useEffect(() => {
        if (formValid) {
            console.log(`Registration data:
                name: ${state.name}
                email: ${state.email}
                password: ${state.password}
                confirm-password: ${state.confirmPassword}
                `);
    
            Api.signup(state.name, state.password);
            setState(initialState);
            setValidation(defaultValidation);
        }
    }, [ formValid]);

    const handleSubmit = (event) => {
        event.preventDefault();        
        setValidation({
            name: validate.nameLength(state.name),
            email: validate.email(state.email),
            password: validate.pass(state.password),
            confirmPassword: validate.confirmPassword(state.password, state.confirmPassword),
        });
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setState((field) => {
            return { ...field, [name]: value };
        });
    };


    return (
        <>
            <main className="main">
                <div className="wrapper">
                    <form onSubmit={handleSubmit}>
                        <h1>Registration</h1>

                        {configForm.map((current, index) => {
                            return (
                                <Input
                                    value={state[current.name]}
                                    name={current.name}
                                    type={current.type}
                                    labelTitle={current.label}
                                    key={`${current.name}_${index}`}
                                    handleInputChange={handleChange}
                                    InputError={validationState[current.name].error}
                                />
                            );
                        })}
                        <input className="btn btn-primary" type="submit" value="Submit" />
                    </form>
                </div>
            </main>
        </>
    );
};

export default Signup;
