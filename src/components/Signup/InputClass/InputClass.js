import React from "react";

class InputClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: "" };
    }    

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }
    
    render() {
        return (
            <p className="field">
            <label>
                Input text:
                <input
                    onChange={this.handleChange}
                    value={this.state.value}
                    name="classInput"
                    type="text"
                />
            </label>
            </p>
        );
    }
}

export default InputClass;