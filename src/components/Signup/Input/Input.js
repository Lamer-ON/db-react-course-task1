import React from "react";

import './../input.scss';

const Input = (props) => {
    return (
        <p className="field">
            <label>{props.labelTitle}</label>
            <input
                value={props.value}
                name={props.name}
                type={props.type}
                placeholder={props.labelTitle}
                onChange={props.handleInputChange}
                onKeyPress={props.keyPress}
            />
        { props.InputError && <span className="error-string error">{props.InputError} </span>}    
        </p>
    )
}

export default Input;