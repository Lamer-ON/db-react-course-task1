import React from "react";
import { useSelector, useDispatch } from 'react-redux'
import { select } from "../redux/features/weather/weatherSlice";

const SelectSity = () => {
    const dispatch = useDispatch()
    const value = useSelector((state) => state.weather.value)
    const cityArr = useSelector((state) => state.weather.dataCity.results)
    const dataCity = useSelector((state) => state.weather.dataCity)
    const loading = useSelector(state => state.loading)
    const loading2 = useSelector(state => state.loading2)

    function handleChange(event) {
        dispatch(select(event.target.value))
    }

    return (
        <form>
            <label>
                Select city:
                <select value={value} onChange={handleChange}>
                    {/* {!loading && !loading2 && dataCity.length != 0 && */}
                    {dataCity &&
                        cityArr.map((city, i) => {
                            return <option value={cityArr[i].name} key={cityArr[i].name}>{cityArr[i].name}</option>
                        })
                    }
                </select>
            </label>
        </form>
    )
}

export default SelectSity;